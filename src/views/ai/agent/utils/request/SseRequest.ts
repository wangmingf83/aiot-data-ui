import { EventSourcePolyfill } from 'event-source-polyfill'
// import {getToken} from "@/utils/auth";
import cache from "@/utils/cache";

// const apiUrl = process.env.VUE_APP_BASE_API
const apiUrl = import.meta.env.VITE_API_URL + '/aidet'

export function linkSseEvent() {
  return new EventSourcePolyfill(`${apiUrl}/sse/connect`, {
    retryOnTimeout: false,
    autoReconnectTimeMs: -1,
    headers: {
      // 'Authorization': 'Bearer ' + cache.getToken(),
      'Authorization': '' + cache.getToken(),
    },
  });
}
