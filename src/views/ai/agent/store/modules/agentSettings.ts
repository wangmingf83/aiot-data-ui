import localCache from "@/views/ai/agent/utils/agentCache";
import { defineStore } from "pinia"

const storageKey = "layout-setting"

const defaultSetting = {
  hiddenStatusLeft: true,
  hiddenStatusSessionList: false,
  ifConc: '0',
  theme: 'theme-light'
}

const storageSetting = JSON.parse(localCache.localCache.get(storageKey)) || ''

export const agentSettings = defineStore('agentSettings', {
  state: () => ({
    hiddenStatusLeft: storageSetting.hiddenStatusLeft === undefined ? defaultSetting.hiddenStatusLeft : storageSetting.hiddenStatusLeft,
    hiddenStatusSessionList: storageSetting.hiddenStatusSessionList === undefined ? defaultSetting.hiddenStatusSessionList : storageSetting.hiddenStatusSessionList,
    ifConc: storageSetting.ifConc === undefined ? defaultSetting.ifConc : storageSetting.ifConc,
    theme: storageSetting.theme === undefined ? defaultSetting.theme : storageSetting.theme
  }),
  actions: {
    setSettingItem(item) {
      let layoutStorageValue = localCache.localCache.get(storageKey);
      if (layoutStorageValue == null || layoutStorageValue === ''){
        layoutStorageValue = JSON.stringify(defaultSetting);
      }
      const obj = JSON.parse(layoutStorageValue);
      obj[item.key] = item.value
      localCache.localCache.set(storageKey,JSON.stringify(obj))
    },
    clearSetting(){
      localCache.localCache.remove(storageKey)
    }
  }
})

