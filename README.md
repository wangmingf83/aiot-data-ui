# ant-ai

# 说明
目前已经不在做这块的业务。全面做大模型应用。
https://gitee.com/dabanzong/guituai-agent

## 注意
由于在开发阶段，频繁提交，可能会有提交遗漏代码或是数据库脚本未及时同步的情况。如遇到请添加微信入群交流或提交Issues。功能会陆续完成开放。

## 介绍
AIOT人工智能物联网平台是一站式物联网开发基础平台，帮助企业快速实现数字化、精细化数据管理。核心系统为：物联网平台 + 数据中台（数据底座）+ AI。
欢迎大家添加最下方的微信入群交流。

  系统包含了缺陷检测、烟火检测、安全帽检测、车流检测、车辆检测、入侵检测、车牌检测、花检测、动物检测、口罩检测、人流检测等等AI模型。并且支持训练、标注等功能。
  同时支持文生图、语音合成等。大模型支持陆续也会慢慢开发。


## 软件架构说明
  本系统采用springboot、spring cloud、vertx、redis、H2、mysql、elasticsearch、TDEngine、RocketMq、flink、hadoop、kafka、Vue3、Mybatis-Plus、minio、k8s等框架和第三方软件，中间件采用模块化无侵入集成非常容易扩展和替换。


## 注意
大模型部分主要是通过api方式调用大厂的大模型。

支持gpt、百川、文生图等

前端地址：  https://gitee.com/wangmingf83/chatgpt-ui

## 系统部分截图

![输入图片说明](images/20240618001336.png)
![输入图片说明](images/20240618001415.png)

![输入图片说明](images/AI1.jpg)
![输入图片说明](images/ai93227.jpg)
  AI功能






## FAQ:
1、目前摄像头部分为调用本地摄像头，仅供参考，后期会修改为调用rtmp、rtsp实时视频。目前前端暂未处理。后台可供参考

2、前端组件不存在错误提示，不用担心，登录系统后在系统菜单中删除这些菜单即可，不影响系统
![输入图片说明](images/2621.jpg)


## 鸣谢声明

该项目使用借鉴了以下优秀开源项目的源码进行改造集成，特此鸣谢声明。

**hugai-chatgpt**

[hugai-chatgpt](https://gitee.com/toushang6015/hugai-chatgpt)

**数据平台**

[数睿通数据中台](https://gitee.com/zrxjava/srt-data-center)

**物联网**

[铱塔智联](https://gitee.com/open-iita/iotkit-parent)


